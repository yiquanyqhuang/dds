﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DDSbeta2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSbeta2.Model.Tests
{
    [TestClass()]
    public class ConvertMsgTests
    {
        [TestMethod()]
        public void HexStringToBytesTest()
        {
            //                   byte[] hexbytes = ConvertMsg.Instance.HexStringToBytes(subInputText);
            var list = new List<int>() { 9, 8, 7 };


            string subInputText = "C3 11 12";

            subInputText += " 0D 0A";
            subInputText = subInputText.Substring(1, subInputText.Length - 1);
            Assert.AreEqual(9, ConvertMsg.Instance.HexStringToBytes(subInputText));
            //Assert.Fail();
        }
    }
}