using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace TestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        public byte[] HexStringToBytes(string hs)
        {
            byte[] a = { 0x00 };            //非靜態變量處理
            try
            {
                //byte[] mid = Encoding.Default.GetBytes(hs);
                string[] strArr = hs.Trim().Split(' ');
                byte[] b = new byte[strArr.Length];
                //逐字變為16進制

                foreach (var word in strArr.Select((value, index) => new { value, index }))
                {
                    b[word.index] = Convert.ToByte(strArr[word.index].PadLeft(2, '0'), 16);
                }
                return b;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public class LogAnalyzer
        {
            public bool IsValidLogFileName(string fileName)
            {
                if (fileName.EndsWith(".SLF")) // INCORRECT HERE
                {
                    return false;
                }
                return true;
            }
        }

        public int HexStrToInt(string HexStr)
        {
            int DevValue = int.Parse(HexStr, System.Globalization.NumberStyles.HexNumber);
            return DevValue;
        }

        [Test]
        public void Test1()
        {
            LogAnalyzer analyzer = new LogAnalyzer();
            bool result = analyzer.IsValidLogFileName("filewithbadextension.foo");

           byte[] WW = HexStringToBytes("C4");
            byte[] bytesS = { 0xC3 };

            Assert.AreEqual(bytesS, WW);


        }
    }
}