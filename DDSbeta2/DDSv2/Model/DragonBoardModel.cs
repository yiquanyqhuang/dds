﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSbeta2.Model
{

    [Serializable]                         
    public class DragonBoardModel : SingletonBase<DragonBoardModel>
    {

        #region Bascily CMD for operating TC100
        public class QuicklyCMD
        {

            public Byte[] GetDragonBoardGPI1Hex = new Byte[] { 0xF4, 0x52, 0x01, 0x01, 0x0D, 0x0A };
            public Byte[] GetDragonBoardGPI2Hex = new Byte[] { 0xF4, 0x52, 0x01, 0x02, 0x0D, 0x0A };
            public Byte[] GetDragonBoardGPI3Hex = new Byte[] { 0xF4, 0x52, 0x01, 0x03, 0x0D, 0x0A };
            public Byte[] GetDragonBoardGPI4Hex = new Byte[] { 0xF4, 0x52, 0x01, 0x04, 0x0D, 0x0A };
            public Byte[] GetDragonBoardGPI5Hex = new Byte[] { 0xF4, 0x52, 0x01, 0x05, 0x0D, 0x0A };


            public string GetDragonBoardGPI1Ascii = "io i 1";
            public string GetDragonBoardGPI2Ascii = "io i 2";
            public string GetDragonBoardGPI3Ascii = "io i 3";
            public string GetDragonBoardGPI4Ascii = "io i 4";
            public string GetDragonBoardGPI5Ascii = "io i 5";
        }
        #endregion
    }
}
