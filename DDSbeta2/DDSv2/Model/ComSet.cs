﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace DDSbeta2.Model
{
    public class SerialComSets 
    {
        public string ComID { get; set; }
     

        public bool comIsUse 
        {
            get; set;
        }
        public int Com { get; set; }
        public int BaudRates { get; set; }
        public int Paritys { get; set; }
        public int Databits { get; set; }
        public int Stopbits { get; set; }
        public int Flowcontrol { get; set; }
        public bool comIsDTR { get; set; }
        public bool comIsRTS { get; set; }

        private static Dictionary<int, Dic_Coms> getComs;
        private static Dictionary<int, Dic_BaudRate> getBaudRate;
        private static Dictionary<int, Dic_Databits> getDatabit;
        private static Dictionary<int, Dic_Stopbit> getStopbit;
        private static Dictionary<int, Dic_Paritys> getParity;
        private static Dictionary<int, Dic_Flowcontrols> getFlowcontrol;
      
        static SerialComSets()
        {
            getComs = new Dictionary<int, Dic_Coms>();

            var CommPorts = new List<string>();
        

            foreach (var New in SerialPort.GetPortNames().Select((value, index) => new { value, index }))
            {
                CommPorts.Add(New.value);
                getComs.Add(New.index, new Dic_Coms { comnumber = New.value });
            }        

            getBaudRate = new Dictionary<int, Dic_BaudRate>()
                {
                    { 0, new Dic_BaudRate {  baudRatenumber  = 4800 } },
                    { 1, new Dic_BaudRate {  baudRatenumber  = 9600 } },
                    { 2, new Dic_BaudRate {  baudRatenumber  = 19200 } },
                    { 3, new Dic_BaudRate {  baudRatenumber  = 38400 } },
                    { 4, new Dic_BaudRate {  baudRatenumber  = 57600 } },
                    { 5, new Dic_BaudRate {  baudRatenumber  = 115200 } },
                    { 6, new Dic_BaudRate {  baudRatenumber  = 230400 } }
                };

            getDatabit = new Dictionary<int, Dic_Databits>()
                {
                    { 0, new Dic_Databits {  databitnumber  = 5 } },
                    { 1, new Dic_Databits {  databitnumber  = 6 } },
                    { 2, new Dic_Databits {  databitnumber  = 7 } },
                    { 3, new Dic_Databits {  databitnumber  = 8 } }
                };
            getStopbit = new Dictionary<int, Dic_Stopbit>()
                {
                    { 0, new Dic_Stopbit {  stopbitnumber  = 1 } },
                    { 1, new Dic_Stopbit {  stopbitnumber  = 2 } },
                    { 2, new Dic_Stopbit {  stopbitnumber  = 1.5 } }
                };
            getFlowcontrol = new Dictionary<int, Dic_Flowcontrols>()
                {
                    { 0, new Dic_Flowcontrols {  flowcontrolname  = "None" } },
                    { 1, new Dic_Flowcontrols {  flowcontrolname  = "RTS" } },
                    { 2, new Dic_Flowcontrols {  flowcontrolname  = "RTS/X" } },
                    { 3, new Dic_Flowcontrols {  flowcontrolname  = "Xon/Xoff" } }
                };
            getParity = new Dictionary<int, Dic_Paritys>()
                {
                    { 0, new Dic_Paritys {  parityname  = "None" } },
                    { 1, new Dic_Paritys {  parityname  = "Odd" } },
                    { 2, new Dic_Paritys {  parityname  = "Even" } },
                    { 3, new Dic_Paritys {  parityname  = "Mark" } },
                    { 4, new Dic_Paritys {  parityname  = "Space" } }
                };

        }
        public static Dictionary<int, Dic_Coms> GetCom
        {
            get { return getComs; }
        }
        public static Dictionary<int, Dic_BaudRate> GetBaudRate
        {
            get { return getBaudRate; }
        }
        public static Dictionary<int, Dic_Databits> GetDatabit
        {
            get { return getDatabit; }
        }

        public static Dictionary<int, Dic_Stopbit> GetStopbit
        {
            get { return getStopbit; }
        }
        public static Dictionary<int, Dic_Paritys> GetParity
        {
            get { return getParity; }
        }

        public static Dictionary<int, Dic_Flowcontrols> GetFlowcontrol
        {
            get { return getFlowcontrol; }
        }  

    }
    public class Dic_Coms : List<string>
    {
        public string comnumber { get; set; }
    }
    public class Dic_BaudRate : List<int>
    {
        public int baudRatenumber { get; set; }
    }
    public class Dic_Stopbit : List<double>
    {
        public double stopbitnumber { get; set; }
    }
    public class Dic_Databits : List<int>
    {
        public int databitnumber { get; set; }
    }

    public class Dic_Paritys : List<string>
    {
        public string parityname { get; set; }
    }
    public class Dic_Flowcontrols : List<string>
    {
        public string flowcontrolname { get; set; }
    }

}
