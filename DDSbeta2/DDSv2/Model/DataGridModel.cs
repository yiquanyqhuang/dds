﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using Microsoft.VisualBasic.FileIO;
using System.Collections.ObjectModel;

namespace DDSbeta2.Model
{

    public class fullFuncion : SingletonBase<fullFuncion>
    {
     
        public string Type { get; set; }      
        public string Com { get; set; }       
        public string MSG { get; set; }
        public string CMD { get; set; }
        public string P1 { get; set; }     
        public string P2 { get; set; }      
        public string P3 { get; set; }     
        public string MSGExcept { get; set; }  
        public string Delay { get; set; }    
        public string Judg { get; set; }

        private ObservableCollection<fullFuncion> _items = new ObservableCollection<fullFuncion>();
        public ObservableCollection<fullFuncion> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;           
            }
        }


        public ObservableCollection<fullFuncion> GetOBC_askform()
        {
            string theFile = null;

            string myPath = @"/script/";
            using (var openFileDialog1 = new System.Windows.Forms.OpenFileDialog())
            {
                // 設定OpenFileDialog屬性
                openFileDialog1.Title = "選擇要開啟的CSV檔案";
                openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*|ods files (*.ods)|*.ods|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.Multiselect = true;

                openFileDialog1.InitialDirectory = myPath;

                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    theFile = openFileDialog1.FileName; //取得檔名
                   
                }
                if (!string.IsNullOrEmpty(theFile))
                {
                    using (TextFieldParser parser = new TextFieldParser(theFile, Encoding.UTF8))
                    {
                        parser.TextFieldType = FieldType.Delimited;
                        parser.SetDelimiters(",");

                        // 헤더값을 List에 넣지 않기 위해 첫줄을 읽도록 한다.
                        parser.ReadFields();
                        while (!parser.EndOfData)
                        {
                            string[] fields = parser.ReadFields();
                            if (fields[0].ToLower().Contains("hex") )
                            {
                                fullFuncion user = new fullFuncion
                                {
                                    Judg = null,
                                    Type = fields[0],     // 번호
                                    Com = fields[1],                   // 이름
                                    MSG = fields[4],               // 사는곳
                                    CMD = string.Concat(fields[7].PadLeft(2,'0')," ", fields[8].PadLeft(2, '0'), " ", fields[9].PadLeft(2, '0'), " ", fields[10].PadLeft(2, '0'),
                                   " ", fields[11].PadLeft(2, '0'), " ", fields[12].PadLeft(2, '0'), " ", fields[13].PadLeft(2, '0'), " ", fields[14].PadLeft(2, '0')),
                                    P1 = fields[3],                    // 직업
                                    P2 = fields[5],                    // 직업
                                    P3 = fields[6],                    // 직업
                                    MSGExcept = fields[15],
                                    Delay = fields[16],

                                };

                               Items.Add(user);

                            }
                            if (fields[0].ToLower().Contains("ascii"))
                            {
                                fullFuncion user = new fullFuncion
                                {
                                    Judg = null,
                                    Type = fields[0],     // 번호
                                    Com = fields[1],                   // 이름
                                    MSG = fields[4],               // 사는곳
                                    CMD = string.Concat(fields[7], " ", fields[8], " ", fields[9], " ", fields[10], " ", fields[11], " ", fields[12], " ", fields[13], " ", fields[14]),
                                    P1 = fields[3],                    // 직업
                                    P2 = fields[5],                    // 직업
                                    P3 = fields[6],                    // 직업
                                    MSGExcept = fields[15],
                                    Delay = fields[16],

                                }; 

                                Items.Add(user);

                            }
                        }

                        return Items;
                    }

                }
                return Items;
            }
            
        }

    }

    public class fullfunctionfilter : SingletonBase<fullfunctionfilter>
    {
        private bool Ishexfun { get; set; }
        public fullfunctionfilter(string[] _sin , bool Ishex = false)
        {


        }
        private string _fulltype;
        public string fulltype
        {
            get
            {
                return _fulltype;
                
            }
            set
            {
                //if (String.IsNullOrEmpty(value))
                _fulltype = value;
               
                    //_fulltype = Ishexfun ? value.PadLeft(2, '0') : value;
            }

        }
        public string Com 
        { get; set; }
        public string MSG
        { get; set; }        
        public string CMD
        { get; set; }
        public string P1
        { get; set; }              
        public string P2
        { get; set; }         
        public string P3
        { get; set; }
        public string MSGExcept
        { get; set; }
        public string Delay 
        { get; set; }

    }
}
