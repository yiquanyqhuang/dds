﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NLog;

namespace DDSbeta2.Model
{

    public class ConvertMsg : SingletonBase<ConvertMsg>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public class ConvertTOOL
        {
      
        }

        /// <summary>
        /// the string (HEX) convert to byte[]
        /// like "C3 C3" -> (byte){0xC3, 0xC3}
        /// </summary>
        /// <param name="hs"></param>
        /// <returns></returns>
        public byte[] HexStringToBytes(string hs)
        {
            byte[] a = { 0x00 };            //非靜態變量處理
            try
            {
                //byte[] mid = Encoding.Default.GetBytes(hs);
                string[] strArr = hs.Trim().Split(' ');
                byte[] b = new byte[strArr.Length];
                //逐字變為16進制

                foreach (var word in strArr.Select((value, index) => new { value, index }))
                {
                    b[word.index] = Convert.ToByte(strArr[word.index].PadLeft(2, '0'), 16);
                }
                return b;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error For HexStringToBytes in ConvertMsg.", MessageBoxButton.OK, MessageBoxImage.Error);
                logger.Log(LogLevel.Error, "EXCEPTION raised: " + ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 由16進制標準表示方法變成ASCII
        //byte array to ASCII & combine Hex string  
        //EX  0x32 0x46 -> 2F
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>   
        public string HexConvertASCII(String hexString)
        {
            try
            {
                string ascii = string.Empty;
                for (int i = 0; i < hexString.Length; i += 2)
                {
                    String hs = string.Empty;
                    hs = hexString.Substring(i, 2);
                    uint decval = System.Convert.ToUInt32(hs, 16);
                    char character = System.Convert.ToChar(decval);
                    ascii += character;
                }
                return ascii;
            }
            catch 
            {; }
            return string.Empty;
        }

        /// <summary>
        /// HexStr Convert to int
        /// Like  0A8C => 2700
        /// </summary>
        /// <param name="HexStr"></param>
        /// <returns></returns>
        public int HexStrToInt(string HexStr)
        {
            int DevValue = int.Parse(HexStr, System.Globalization.NumberStyles.HexNumber);
            return DevValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public byte HexToByte(string hex)
        {
            if (hex.Length > 2 || hex.Length <= 0)
                throw new ArgumentException("hex must be 1 or 2 characters in length");
            byte newByte = byte.Parse(hex, System.Globalization.NumberStyles.HexNumber);
            return newByte;
        }

        public string ASCIIToHex(string ascii)
        {
            StringBuilder sb = new StringBuilder();
            byte[] inputBytes = Encoding.UTF8.GetBytes(ascii);
            foreach (byte b in inputBytes)
            {
                sb.Append(string.Format("{0:X2} ", b));
            }
            return sb.ToString();
        }

    }

}
