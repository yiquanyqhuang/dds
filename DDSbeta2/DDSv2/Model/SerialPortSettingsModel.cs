﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Management;
using Microsoft.Win32;
using System.Collections.ObjectModel;
using System.IO;

namespace DDSbeta2.Model
{


    [Serializable]
    public class SerialPortSettingsModel : SingletonBase<SerialPortSettingsModel>
    {
        #region TC100  
        public enum comlist
        {
            comportA = 0,
            comportB = 1,
            comportC = 2,
            comportD = 3,
            comportE = 4,

        }

        #endregion

        public class Operation_mode : List<string>
        {
            public string Name { get; set; }
        }
       

        public SerialPortSettingsModel()
        {

        }
   
        public string XmlName { get; set; }
        public List<SerialPortSettingsModel> CheckXMLfileInScript()
        {
            List<SerialPortSettingsModel> InscriptXMLs = new List<SerialPortSettingsModel>();
            string folderName = @"script\_run";
            DirectoryInfo di = new DirectoryInfo(folderName);
            InscriptXMLs.Clear();
            foreach (var fi in di.GetFiles("*.xml"))
            {
                InscriptXMLs.Add(new SerialPortSettingsModel()
                {
                    XmlName = fi.Name.Replace(".xml", "")
                });
            }
            return InscriptXMLs;
        }


        public static string byteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {
                //Parallel.ForEach(bytes, (d) =>
                //{
                //    //returnStr = Convert.ToString(bytes[i], 16);
                //    returnStr += d.ToString("X2");
                //    returnStr += d.ToString(" ");
                //});                  
                for (int i = 0; i < bytes.Length; i++)
                {
                    //returnStr = Convert.ToString(bytes[i], 16);
                    returnStr += bytes[i].ToString("X2");
                    returnStr += bytes[i].ToString(" ");
                }
            }
            return returnStr.Trim();
        }

        #region TC100  
        public enum ComportFunc
        {
            TC100 = 0,
            Sensing = 1,
            DB1 = 2,
            DB2 = 3,
            DB3 = 4,
            General1 = 5
        }      

        #endregion

        #region Comm. Port    
        public string DeviceID { get; set; }
        //public string Description { get; set; }
        public string DeviceInfo { get; set; }

        /// <summary>
        /// Get the comport all comport list.
        /// </summary>
        /// <returns></returns>
        public List<SerialPortSettingsModel> getCommPorts()
        {
            List<SerialPortSettingsModel> devices = new List<SerialPortSettingsModel>();
            ///add the reference function

            using (ManagementClass i_Entity = new ManagementClass("Win32_PnPEntity"))
            {
                foreach (ManagementObject i_Inst in i_Entity.GetInstances())
                {
                    Object o_Guid = i_Inst.GetPropertyValue("ClassGuid");
                    if (o_Guid == null || o_Guid.ToString().ToUpper() != "{4D36E978-E325-11CE-BFC1-08002BE10318}")
                        continue; // Skip all devices except device class "PORTS"

                    String s_Caption = i_Inst.GetPropertyValue("Caption").ToString();
                    String s_Manufact = i_Inst.GetPropertyValue("Manufacturer").ToString();
                    String s_DeviceID = i_Inst.GetPropertyValue("PnpDeviceID").ToString();
                    String s_RegPath = "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Enum\\" + s_DeviceID + "\\Device Parameters";
                    String s_PortName = Registry.GetValue(s_RegPath, "PortName", "").ToString();

                    int s32_Pos = s_Caption.IndexOf(" (COM");
                    if (s32_Pos > 0) // remove COM port from description
                        s_Caption = s_Caption.Substring(0, s32_Pos);

                  
                    devices.Add(new SerialPortSettingsModel()
                    {

                        DeviceID = s_PortName,                    
                        //Description = index.ToString(),//(string)device.GetPropertyValue("Description"),
                        /// display the comport info 
                        /// 1. the row of combobox clearly display detail info
                        //DeviceInfo = (string)device.GetPropertyValue("Description") + " (" + (string)device.GetPropertyValue("DeviceID") + ")"
                        /// 2. the row of combobox display the simple info
                        DeviceInfo = s_PortName//(string)device.GetPropertyValue("DeviceID")

                    });

                }
                 i_Entity.Dispose();
            }

            return devices;

        }

   

        #endregion

        #region Baud Rate
        public string BaudRateName { get; set; }
        public int BaudRateValue { get; set; }

        public List<SerialPortSettingsModel> getBaudRates()
        {
            List<SerialPortSettingsModel> returnBaudRates = new List<SerialPortSettingsModel>();
            returnBaudRates.Add(new SerialPortSettingsModel() { BaudRateName = "4800 baud", BaudRateValue = 4800 });
            returnBaudRates.Add(new SerialPortSettingsModel() { BaudRateName = "9600 baud", BaudRateValue = 9600 });
            returnBaudRates.Add(new SerialPortSettingsModel() { BaudRateName = "19200 baud", BaudRateValue = 19200 });
            returnBaudRates.Add(new SerialPortSettingsModel() { BaudRateName = "38400 baud", BaudRateValue = 38400 });
            returnBaudRates.Add(new SerialPortSettingsModel() { BaudRateName = "57600 baud", BaudRateValue = 57600 });
            returnBaudRates.Add(new SerialPortSettingsModel() { BaudRateName = "115200 baud", BaudRateValue = 115200 });
            returnBaudRates.Add(new SerialPortSettingsModel() { BaudRateName = "230400 baud", BaudRateValue = 230400 });
            return returnBaudRates;
        }
        #endregion

        #region Parity
        public string ParityName { get; set; }
        public Parity ParityValue { get; set; }

        public List<SerialPortSettingsModel> getParities()
        {
            List<SerialPortSettingsModel> returnParities = new List<SerialPortSettingsModel>();
            returnParities.Add(new SerialPortSettingsModel() { ParityName = "Even", ParityValue = Parity.Even });
            returnParities.Add(new SerialPortSettingsModel() { ParityName = "Mark", ParityValue = Parity.Mark });
            returnParities.Add(new SerialPortSettingsModel() { ParityName = "None", ParityValue = Parity.None });
            returnParities.Add(new SerialPortSettingsModel() { ParityName = "Odd", ParityValue = Parity.Odd });
            returnParities.Add(new SerialPortSettingsModel() { ParityName = "Space", ParityValue = Parity.Space });
            return returnParities;
        }
        #endregion

        #region DataBits
        public int[] getDataBits = { 5, 6, 7, 8 };
        #endregion

        #region StopBits
        public string StopBitsName { get; set; }
        public StopBits StopBitsValue { get; set; }

        public List<SerialPortSettingsModel> getStopBits()
        {
            List<SerialPortSettingsModel> returnStopBits = new List<SerialPortSettingsModel>();
            returnStopBits.Add(new SerialPortSettingsModel() { StopBitsName = "None", StopBitsValue = StopBits.None });
            returnStopBits.Add(new SerialPortSettingsModel() { StopBitsName = "One", StopBitsValue = StopBits.One });
            returnStopBits.Add(new SerialPortSettingsModel() { StopBitsName = "OnePointFive", StopBitsValue = StopBits.OnePointFive });
            returnStopBits.Add(new SerialPortSettingsModel() { StopBitsName = "Two", StopBitsValue = StopBits.Two });
            return returnStopBits;
        }
        #endregion

        #region Line Ending
        public string LineEndingName { get; set; }
        public string LineEndingChars { get; set; }

        public List<SerialPortSettingsModel> getLineEndings()
        {
            List<SerialPortSettingsModel> returnLineEndings = new List<SerialPortSettingsModel>();
            returnLineEndings.Add(new SerialPortSettingsModel() { LineEndingName = "No line ending", LineEndingChars = "" });
            returnLineEndings.Add(new SerialPortSettingsModel() { LineEndingName = "Newline", LineEndingChars = "\n" });
            returnLineEndings.Add(new SerialPortSettingsModel() { LineEndingName = "Carriage return", LineEndingChars = "\r" });
            returnLineEndings.Add(new SerialPortSettingsModel() { LineEndingName = "Both NL & CR", LineEndingChars = "\r\n" });
            return returnLineEndings;
        }
        #endregion

        public string[] StringSeparators = new string[] { "\r\n","\r","\n","\t"};

        public char[] CharSeparators = {',', '.', ':', '\t' ,'*', '/','\r','\n','+','-'};

        /// <summary>
        ///  用 string [] list 當作集合使每個string 後面都可以加入空白
        /// </summary>
        /// <param name="_List"></param>
        /// <returns></returns>
        public string AddSpace(List<string> _List)
        {
            string addspace = null;
            foreach (var item in _List)
            {
                addspace += string.Concat(item, " ");
            }
            return addspace.Trim();
        }

        public string ReceivedDataTOInfoTable( SerialPort _com,string _msgtype ,string _msg)
        {
            string returnMsg = null;
            String Timestamp = null;
            Timestamp = DateTime.Now.ToString("MMdd-hhmmss-fff");
            string portname = string.Concat("[", _com.PortName, "]");
            string msgtype = string.Concat("[", _msgtype, "]");
            returnMsg = AddSpace(new List<string> { Timestamp, portname, msgtype, _msg });
            
            return returnMsg ;
        }


    }



}
