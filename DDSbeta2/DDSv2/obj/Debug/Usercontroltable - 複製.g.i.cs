﻿#pragma checksum "..\..\Usercontroltable - 複製.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "FA9BB22742F37C253B074BC33268BC13ED1C8BAF"
//------------------------------------------------------------------------------
// <auto-generated>
//     這段程式碼是由工具產生的。
//     執行階段版本:4.0.30319.42000
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     變更將會遺失。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ToyoTc100;


namespace ToyoTc100 {
    
    
    /// <summary>
    /// Usercontroltable
    /// </summary>
    public partial class Usercontroltable : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 30 "..\..\Usercontroltable - 複製.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cb_tc100coms;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\Usercontroltable - 複製.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cb_tc100baudrate;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\Usercontroltable - 複製.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cb_tc100com;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\Usercontroltable - 複製.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cb_arduinocombaudrate;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\Usercontroltable - 複製.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cb_arduinocom;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\Usercontroltable - 複製.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cb_arduino;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\Usercontroltable - 複製.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox ReceiveTextlist;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\Usercontroltable - 複製.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tb_CMDscript;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\Usercontroltable - 複製.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bt_correctionpos;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\Usercontroltable - 複製.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bt_sendCMDfromTB;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ToyoTc100;component/usercontroltable%20-%20%e8%a4%87%e8%a3%bd.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Usercontroltable - 複製.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.cb_tc100coms = ((System.Windows.Controls.CheckBox)(target));
            
            #line 31 "..\..\Usercontroltable - 複製.xaml"
            this.cb_tc100coms.Checked += new System.Windows.RoutedEventHandler(this.cb_tc100_Checked);
            
            #line default
            #line hidden
            return;
            case 2:
            this.cb_tc100baudrate = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.cb_tc100com = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 4:
            this.cb_arduinocombaudrate = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.cb_arduinocom = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.cb_arduino = ((System.Windows.Controls.CheckBox)(target));
            
            #line 52 "..\..\Usercontroltable - 複製.xaml"
            this.cb_arduino.Click += new System.Windows.RoutedEventHandler(this.cb_arduino_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.ReceiveTextlist = ((System.Windows.Controls.ListBox)(target));
            return;
            case 8:
            this.tb_CMDscript = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.bt_correctionpos = ((System.Windows.Controls.Button)(target));
            
            #line 92 "..\..\Usercontroltable - 複製.xaml"
            this.bt_correctionpos.Click += new System.Windows.RoutedEventHandler(this.bt_correctionpos_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.bt_sendCMDfromTB = ((System.Windows.Controls.Button)(target));
            
            #line 93 "..\..\Usercontroltable - 複製.xaml"
            this.bt_sendCMDfromTB.Click += new System.Windows.RoutedEventHandler(this.bt_sendCMDfromTB_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

