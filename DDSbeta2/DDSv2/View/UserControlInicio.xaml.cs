﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DDSbeta2.ViewModel;
using System.Data;
using System.Xml;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using DDSbeta2.Model;


namespace DDSbeta2
{
    /// <summary>
    /// Interação lógica para UserControlInicio.xam
    /// </summary>
    /// 
    public partial class UserControlInicio : UserControl
    {
        public class TC100FuncParac
        {

            /// <summary>
            /// ID
            /// </summary>
            public int ID { get; set; }
            /// <summary>
            /// script 功能選擇
            /// </summary>
            public string FuncType { get; set; }

            private static Dictionary<int, Operation_mode> getAll;

            static TC100FuncParac()
            {
                getAll = new Dictionary<int, Operation_mode>()
                {
                    { 0, new Operation_mode {  Name  = "INC" } },
                    { 1, new Operation_mode {  Name  = "ABS" } },
                    { 2, new Operation_mode {  Name  = "ORG" } },
                    { 3, new Operation_mode {  Name  = "TLS+" } },
                    { 4, new Operation_mode {  Name  = "TLS-" } },
                    { 5, new Operation_mode {  Name  = "INC-R" } },
                    { 6, new Operation_mode {  Name  = "ABS-R" } },
                    { 7, new Operation_mode {  Name  = "INC-T" } },
                    { 8, new Operation_mode {  Name  = "ABS-T" } },
                    {  9, new Operation_mode {  Name  = "UART-G" } },
                    { 10, new Operation_mode {  Name  = "UART-DB1" } },
                    { 11, new Operation_mode {  Name  = "UART-DB2" } },
                    { 12, new Operation_mode {  Name  = "UART-DB3" } }
                };
            }
            public static Dictionary<int, Operation_mode> GetAll
            {
                get { return getAll; }
            }

            public int OperaMode { get; set; }
            /// <summary>
            /// 位置
            /// </summary>
            public double AnchorPosition { get; set; }

            /// <summary>
            /// 速度
            /// </summary>
            public int AnchorSpeed { get; set; }

            /// <summary>
            ///  閒置時間
            /// </summary>
            public int AnchorDelay { get; set; }

            /// <summary>
            /// 是否被記錄
            /// </summary>
            public bool AnchorBeRecord { get; set; }
            public string ScriptNote { get; set; }
            public int NextStride { get; set; }
        }
        public UserControlInicio()
        {
            InitializeComponent();
         

        }
        private void ExportingThescription_Click(object sender, RoutedEventArgs e)
        {
        

        }
        private void SaveRuningScriptlist_Click(object sender, RoutedEventArgs e)
        {              
            

        }

        private void ClearRuningScriptlist_Click(object sender, RoutedEventArgs e)
        { 
        }

        private void LoadRuningScriptlist_Click(object sender, RoutedEventArgs e)
        {
           
        }
        private void LoadDefaultScript_Click(object sender, RoutedEventArgs e)
        {

        

        }
        
        private void SaveDataToXml(DataGrid _obj, string path)
        {
        
        }

        private void bt_check_Click(object sender, RoutedEventArgs e)
        {
            string PathScript_run = null;
            string selectfile = Listscripts.SelectedValue.ToString();
            selectfile = PathScript_run + selectfile + ".ini";
            loadscript(selectfile);
            SetScriptGrid.Visibility = Visibility.Hidden;
        }

        private void bt_cancel_Click(object sender, RoutedEventArgs e)
        {
            SetScriptGrid.Visibility = Visibility.Hidden;
        }


       
        public class IniFile
        {
            private string filePath;
            private StringBuilder lpReturnedString;
            private int bufferSize;

            [DllImport("kernel32")]
            private static extern long WritePrivateProfileString(string section, string key, string lpString, string lpFileName);

            [DllImport("kernel32")]
            private static extern int GetPrivateProfileString(string section, string key, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

            public IniFile(string iniPath)
            {
                filePath = iniPath;
                bufferSize = 512;
                lpReturnedString = new StringBuilder(bufferSize);
            }

            // read ini date depend on section and key
            public string ReadIniFile(string section, string key, string defaultValue)
            {
                lpReturnedString.Clear();
                GetPrivateProfileString(section, key, defaultValue, lpReturnedString, bufferSize, filePath);
                return lpReturnedString.ToString();
            }

            // write ini data depend on section and key
            public void WriteIniFile(string section, string key, Object value)
            {
                WritePrivateProfileString(section, key, value.ToString(), filePath);
            }

        }


        private void loadscript(string _path)
        {
           


        }
       



    }
    public class Operation_mode : List<string>
    {
        public string Name { get; set; }
    }
   





}
