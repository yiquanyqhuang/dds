﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.IO.Ports;
using DDSbeta2.ViewModel;
using System.Collections.ObjectModel;


namespace DDSbeta2
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    ///


    public partial class MainWindow : Window
    {
        public SerialPort[] mul_SerialPort;
        public Dictionary<string, SerialPort> mul_com;
        public string indebugpath = System.Environment.CurrentDirectory;
        UserControlInicio tc100ControlledTable = new UserControlInicio();
        Usercontroltable ComportTable = new Usercontroltable();
        UsercontrolCom SerialTestForm = new UsercontrolCom();
        UserControlEscolha testform = new UserControlEscolha();



        public MainWindow()
        {

            InitializeComponent();
            SerialPortViewModel viewModel = new SerialPortViewModel();
            this.DataContext = viewModel;

            if (Directory.Exists(@"script") == false) Directory.CreateDirectory( @"script");
            if (Directory.Exists(@"script/_default") == false) Directory.CreateDirectory(@"script/_default");
            if (Directory.Exists(@"script/_run") == false) Directory.CreateDirectory(@"script/_run");
            if (Directory.Exists(@"img") == false) Directory.CreateDirectory(@"img");
            if (Directory.Exists(@"param") == false) Directory.CreateDirectory(@"param");
            if (Directory.Exists(@"util") == false) Directory.CreateDirectory(@"util");

             string bootpath = System.Environment.CurrentDirectory + @"\script\";
 

            FileTree.Items.Clear();
            /*添加material文件夾目錄*/
            var mtrFiles = Directory.GetFiles(bootpath + "/_run", ".xml");
            TreeViewItem mtrNode = new TreeViewItem();
            mtrNode.Header = "材料庫";

            foreach (string fileName in mtrFiles)
            {
                TreeViewItem item = new TreeViewItem();
                item.Header = fileName.Substring((bootpath).Length + 10);
                mtrNode.Items.Add(item);
            }
            FileTree.Items.Add(mtrNode);

            /*添加rfa文件夾目錄*/
            var rfaFiles = Directory.GetFiles(bootpath + "/_run", "*.xml");
            TreeViewItem rfaNode = new TreeViewItem();
            rfaNode.Header = "族庫";

            foreach (string fileName in rfaFiles)
            {
                TreeViewItem item = new TreeViewItem();
                item.Header = fileName.Substring((bootpath).Length + 5);
                rfaNode.Items.Add(item);
            }
            FileTree.Items.Add(rfaNode);


            TreeViews.Items.Clear();
            /*添加rfa文件夾目錄*/
            var XMLFiles = Directory.GetFiles(bootpath + "/_run", "*.xml");
            TreeViewItem XMLsFiles = new TreeViewItem();
            XMLsFiles.Header = "Script";

            foreach (string s in Directory.GetFiles(@"script/_run/"))
            {
                string xmlname = s.Replace("script/_run/","");
                TreeViewItem item = new TreeViewItem();
                item.Header = xmlname;
                item.Tag = xmlname;
                item.FontWeight = FontWeights.Normal;
                //item.Items.Add("script");
                //item.Expanded += new RoutedEventHandler(folder_Expanded);
                XMLsFiles.Items.Add(item);
            }
          
            TreeViews.Items.Add(XMLsFiles);
        }
      
        public class MenuItem
        {
            public MenuItem()
            {
                this.Items = new ObservableCollection<MenuItem>();
            }

            public string Title { get; set; }

            public ObservableCollection<MenuItem> Items { get; set; }
        }

        void folder_Expanded(object sender, RoutedEventArgs e)

        {

            TreeViewItem item = (TreeViewItem)sender;

            if (item.Items.Count == 1 && item.Items[0] == "script")

            {

                item.Items.Clear();

                try

                {

                    foreach (string s in Directory.GetDirectories(item.Tag.ToString()))

                    {

                        TreeViewItem subitem = new TreeViewItem();

                        subitem.Header = s.Substring(s.LastIndexOf("\\") + 1);

                        subitem.Tag = s;

                        subitem.FontWeight = FontWeights.Normal;

                        subitem.Items.Add("script");

                        subitem.Expanded += new RoutedEventHandler(folder_Expanded);

                        item.Items.Add(subitem);

                    }

                }

                catch (Exception) { }

            }

        }

        public void TreeViewItem_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = e.Source as TreeViewItem;
            if ((item.Items.Count == 1) && (item.Items[0] is string))
            {
                item.Items.Clear();

                DirectoryInfo expandedDir = null;
                if (item.Tag is DriveInfo)
                    expandedDir = (item.Tag as DriveInfo).RootDirectory;
                if (item.Tag is DirectoryInfo)
                    expandedDir = (item.Tag as DirectoryInfo);
                try
                {
                    foreach (DirectoryInfo subDir in expandedDir.GetDirectories())
                        item.Items.Add(CreateTreeItem(subDir));
                }
                catch { }
            }
        }

        private TreeViewItem CreateTreeItem(object o)
        {
            TreeViewItem item = new TreeViewItem();
            item.Header = o.ToString();
            item.Tag = o;
            item.Items.Add("Loading...");
            return item;
        }


        private void ButtonFechar_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ListViewMenu.SelectedIndex;
            MoveCursorMenu(index);

            switch (index)
            {
                case 0:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(ComportTable);
                    break;
                case 1:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(tc100ControlledTable);
                    break;
                case 2:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(testform);
                    break;
                case 3:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(SerialTestForm);
                    break;
                default:
                    break;
            }
        }

        private void MoveCursorMenu(int index)
        {
            TrainsitionigContentSlide.OnApplyTemplate();
            GridCursor.Margin = new Thickness(0, (0 + (60 * index)), 0, 0);
        }

        public void Okok_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(Name, Value);
        }



    }

}
