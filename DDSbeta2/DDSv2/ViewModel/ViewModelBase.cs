﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.Windows.Data;

namespace DDSbeta2.ViewModel
{
    public class ViewModelBase : INotifyPropertyChanged, IDisposable
    {

        protected ViewModelBase() { }

        #region Debugging Aides

        /// <summary>
        /// Warns the developer if this object does not have
        /// a public property with the specified name. This 
        /// method does not exist in a Release build.
        /// </summary>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public virtual void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,  
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                    throw new Exception(msg);
                else
                    Debug.Fail(msg);
            }
        }

        /// <summary>
        /// Returns whether an exception is thrown, or if a Debug.Fail() is used
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// The default value is false, but subclasses used by unit tests might 
        /// override this property's getter to return true.
        /// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }

        #endregion // Debugging Aides

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raises the PropertyChange event for the property specified
        /// </summary>
        /// <param name="propertyName">Property name to update. Is case-sensitive.</param>
        public virtual void RaisePropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);
            OnPropertyChanged(propertyName);
        }

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);

            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        #endregion // INotifyPropertyChanged Members

        #region IDisposable Members

        bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ViewModelBase()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }

        #endregion // IDisposable Members

        protected void OnPropertyChangeds([CallerMemberName] string PropertyName = null)
        {
            if (PropertyName != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }

        public CollectionViewSource cvs = new CollectionViewSource();
        public ObservableCollection<Model.SerialComSets> col;
        public ICollectionView View
        {
            get
            {
                cvs = new CollectionViewSource();
                if (cvs.Source == null)
                {
                    GetData();
                    cvs.View.CurrentChanged += (sender, e) => Detail = cvs.View.CurrentItem as Model.SerialComSets;
                }
                return cvs.View;
            }
        }

        private Model.SerialComSets _detail = null;
        public Model.SerialComSets Detail { get => this._detail; set { this._detail = value; OnPropertyChangeds(); } }
        List<string> comidlist = new List<string>() { "comA", "comB", "comC", "comD", "comE", "comF" };
        private void GetData()
        {
          
            col = new ObservableCollection<Model.SerialComSets>();
            foreach (var com in comidlist)
            {

                col.Add(new Model.SerialComSets()
                {
                    ComID = com,
                    comIsUse = false,
                    Com = 0,
                    BaudRates = 5,
                    Paritys = 0,
                    Databits = 3,
                    Stopbits = 0,
                    Flowcontrol = 0,
                    comIsDTR = true,
                    comIsRTS = true
                });

            }
            cvs.Source = col;
        }



    }
}
