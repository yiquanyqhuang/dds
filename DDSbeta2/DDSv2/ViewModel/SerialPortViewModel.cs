﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.ComponentModel;
using System.IO.Ports;
using System.Windows.Input;
using System.Diagnostics;
using NLog;
using System.Windows;
using System.Reflection;
using Microsoft.WindowsAPICodePack.Dialogs;
using DDSbeta2.Model;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.Data;
using System.Threading;
using Microsoft.VisualBasic.FileIO;



namespace DDSbeta2.ViewModel
{
    class SerialPortViewModel : ViewModelBase
    {
        #region Public Properties

        private DragonBoardModel.QuicklyCMD DragonBoardCMD = new DragonBoardModel.QuicklyCMD();
        delegate void Display(string _data);
        ObservableCollection<Model.SerialComSets> _seriallist = new ObservableCollection<Model.SerialComSets>();
        List<string> comidlist = new List<string>() { "comA", "comB", "comC", "comD", "comE", "comF" };
        
        public ObservableCollection<Model.SerialComSets> seriallist
        {
            get { return _seriallist; }
            set
            {
                _seriallist = value;
                       
                RaisePropertyChanged("seriallist");
            }
        }
        public string InputText { get; set; }
        public string OutputText { get; set; }
        public string WindowTitle { get; private set; }
        public List<SerialPortSettingsModel> CommPorts { get; set; }
        public SerialPortSettingsModel SelectedCommPort { get; set; }
        public SerialPortSettingsModel SelectedGeneralCommPort { get; set; }
        public SerialPortSettingsModel SelectedDB1CommPort { get; set; }
        public SerialPortSettingsModel SelectedDB2CommPort { get; set; }
        public SerialPortSettingsModel SelectedDB3CommPort { get; set; }

        public List<SerialPortSettingsModel> BaudRates { get; set; }
        public int SelectedBaudRate { get; set; }

        public List<SerialPortSettingsModel> Parities { get; private set; }
        public Parity SelectedParity { get; set; }
        public List<SerialPortSettingsModel> StopBitsList { get; private set; }
        public StopBits SelectedStopBits { get; set; }
        public int[] DataBits { get; set; }
        public int SelectedDataBits { get; set; }
        public List<SerialPortSettingsModel> LineEndings { get; private set; }
        public string SelectedLineEnding { get; set; }
        public bool IsDTR { get; set; }
        public bool IsRTS { get; set; }
        //public bool EnableDisableSettings { get; private set; }
        public string FileLocation { get; private set; }
        public class BindingProxy : Freezable
        {

            protected override Freezable CreateInstanceCore()
            {
                return new BindingProxy();
            }

            public object Data
            {
                get { return (object)GetValue(DataProperty); }
                set { SetValue(DataProperty, value); }
            }

            public static readonly DependencyProperty DataProperty =
             DependencyProperty.Register("Data", typeof(object),
                    typeof(BindingProxy));
        }
        public SerialPortSettingsModel SelectedXML { get; set; }
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public string[] FileExtensions { get; private set; }
        public string SelectedFileExtension { get; private set; }
        public string FileName
        {
            get { return _FileName; }
            set
            {
                _FileName = value;
                OnPropertyChanged("ExportFile");
            }
        }
        public string ExportStatus { get; private set; }
        public bool ExportStatusSuccess { get; private set; }

        // Disable interaction from UI
        // TODO: Trigger TextBoxAutomaticScrollingExtension.cs or Scroll to
        //       end the TextBox when CheckBox is checked for second time.
        public string ScrollConfirm
        {
            get
            {
                // Debug only
                //return "Autoscroll (" + ScrollOnTextChanged.ToString() + ")";
                return "Autoscroll";
            }
        }
        public bool ScrollOnTextChanged { get; private set; }
        public bool IsAutoscrollChecked
        {
            get { return _IsAutoscrollChecked; }
            set
            {
                _IsAutoscrollChecked = value;
                if (_IsAutoscrollChecked == true)
                {
                    ScrollOnTextChanged = true;
                }
                else
                {
                    ScrollOnTextChanged = false;
                }

                OnPropertyChanged("IsAutoscrollChecked");
                OnPropertyChanged("ScrollOnTextChanged");
                OnPropertyChanged("ScrollConfirm");
            }
        }
        #endregion

        #region DDS Properties
        public bool EnableDisableComASettings { get; private set; }
        public bool EnableDisableComBSettings { get; private set; }
        public bool EnableDisableComCSettings { get; private set; }
        public bool EnableDisableComDSettings { get; private set; }
        public bool EnableDisableComESettings { get; private set; }
        public bool IsUsedComA { get; set; }
        public bool IsUsedComB { get; set; }
        public bool IsUsedComC { get; set; }
        public bool IsUsedComD { get; set; }
        public bool IsUsedComE { get; set; }
        public SerialPortSettingsModel SelectedComA { get; set; }
        public SerialPortSettingsModel SelectedComB { get; set; }
        public SerialPortSettingsModel SelectedComC { get; set; }
        public SerialPortSettingsModel SelectedComD { get; set; }
        public SerialPortSettingsModel SelectedComE { get; set; }

        public SerialPortSettingsModel SelectedTestCom { get; set; }
        public int SelectedComABaudRate { get; set; }
        public int SelectedComBBaudRate { get; set; }
        public int SelectedComCBaudRate { get; set; }
        public int SelectedComDBaudRate { get; set; }
        public int SelectedComEBaudRate { get; set; }
        #endregion


        #region serial class ini
        public SerialPortViewModel()
        {
            logger.Log(LogLevel.Debug, "[Notify] " + "[Nul] " + "--- PROGRAM STARTED ---");

            _SerialPort = new SerialPort();
            SerialPortComA = new SerialPort();
            SerialPortComB = new SerialPort();
            SerialPortComC = new SerialPort();
            SerialPortComD = new SerialPort();
            SerialPortComE = new SerialPort();

            logger.Log(LogLevel.Debug, "[Notify] " + "[Nul] " + "New instance of SerialPort() is initialized.");
          
            // Get lists of settings objects
            try
            {
                CommPorts = SerialPortSettingsModel.Instance.getCommPorts();
                BaudRates = SerialPortSettingsModel.Instance.getBaudRates();
                Parities = SerialPortSettingsModel.Instance.getParities();
                DataBits = SerialPortSettingsModel.Instance.getDataBits;
                StopBitsList = SerialPortSettingsModel.Instance.getStopBits();
                LineEndings = SerialPortSettingsModel.Instance.getLineEndings();
                FileExtensions = FileExportSettingsModel.Instance.getFileExtensions;
                logger.Log(LogLevel.Debug, "[Notify] " + "[Nul] "+ "All lists of settings objects are loaded.");
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Error, "[Error] " + "EXCEPTION raised: " + ex.ToString());
            }

            // Set default values
            if (CommPorts != null & CommPorts.Count != 0)
            {
                SelectedCommPort = CommPorts[0];
                SelectedComA = CommPorts[0];
                SelectedComB = CommPorts[0];
                SelectedComC = CommPorts[0];
                SelectedComD = CommPorts[0];
                SelectedComE = CommPorts[0];
                SelectedTestCom = CommPorts[0];

            }
            SelectedBaudRate = 19200;
            SelectedComABaudRate = 115200;
            SelectedComBBaudRate = 115200;
            SelectedComCBaudRate = 115200;
            SelectedComDBaudRate = 115200;
            SelectedComEBaudRate = 115200;
          
            SelectedParity = Parity.None;
            SelectedDataBits = 8;
            SelectedStopBits = StopBits.One;
            SelectedLineEnding = "";
            IsDTR = true;
            IsRTS = true;
            IsUsedComA = false;
            IsUsedComB = false;
            IsUsedComC = false;
            IsUsedComD = false;
            IsUsedComE = false;
            EnableDisableComASettings = true;
            EnableDisableComBSettings = true;
            EnableDisableComCSettings = true;
            EnableDisableComDSettings = true;
            EnableDisableComESettings = true;
            FileLocation = AssemblyDirectory;
            SelectedFileExtension = FileExtensions[0];
            //WindowTitle = AppTitle + " (" + GetConnectionStatus() + ")";

            ScrollOnTextChanged = true;
            logger.Log(LogLevel.Debug, "[Notify] " + "[Nul] " + "All default values are set. End of SerialCommViewModel() constructor!");

           

        }

        #endregion

        #region serial comport setting
        #region comA

        #endregion
        #region comB
        #endregion
        #region comC
        #endregion
        #region comD
        #endregion
        #region comE
        #endregion
        #region comF
        #endregion
        #endregion

        #region Public methods

       
        
        #endregion

        #region Private Fields

        private static string AppTitle = "SerialComm Monitor V2";
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private SerialPort _SerialPort ;

        private DispatcherTimer timer = null;
        private string _FileName = "output_data";
        private bool _IsAutoscrollChecked = true;

        private ICommand _Open;
        private ICommand _Close;
        private ICommand _Send;
        private ICommand _Clear;
        private ICommand _OpenLink;
        private ICommand _RefreshPorts;
        private ICommand _ChangeFileLocation;
        private ICommand _ExportTXTFile;
        private ICommand _RestoreSetCom;

        private ICommand _command;
        public ICommand RemoveCommand
        {
            get
            {
                if (_command == null)
                {
                    _command = new DelegateCommand(CanExecute, Execute);

                    OnPropertyChanged("Items");
                }
                return _command;
            }
        }
        private void Execute(object parameter)
        {
            int index = Items.IndexOf(parameter as Model.fullFuncion);
            if (index > -1 && index < Items.Count)
            {
                //Items.RemoveAt(index);
                //MessageBox.Show(Items[1].Type);
                Items[index].Judg = "failt";

                //OnPropertyChanged("Items");                 

                OnPropertyChanged("RemoveCommand");
              
            }
            //TheSriptionMenu
        }                  
       
        private bool CanExecute(object parameter)
        {
           
            return true;
        }

      
        #endregion

        #region Private DDS Fields
        private ObservableCollection<fullFuncion> _items = new ObservableCollection<fullFuncion>();
        public ObservableCollection<fullFuncion> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                OnPropertyChanged("Items");
            }
        }

        private SerialPort SerialPortComA;
        private SerialPort SerialPortComB;
        private SerialPort SerialPortComC;
        private SerialPort SerialPortComD;
        private SerialPort SerialPortComE;

        private ICommand _EnablesUseComA;
        private ICommand _EnablesUseComB;
        private ICommand _EnablesUseComC;
        private ICommand _EnablesUseComD;
        private ICommand _EnablesUseComE;

        private ICommand _Loadfullfunctionscript;
        #endregion

        #region Private DDS ICommands
        public ICommand Loadfullfunctionscript
        {
            get
            {
                _Loadfullfunctionscript = new RelayCommand(
                             param => math_Loadfullfunctionscript());
                return _Loadfullfunctionscript;
            }
        }
        public ICommand EnablesUseComA
        {
            get
            {
                _EnablesUseComA = new RelayCommand(
                             param => math_EnablesUseCom(IsUsedComA, SerialPortComA, SerialPortSettingsModel.comlist.comportA));
                  //param => StartListening(SerialPortComA, SerialPortSettingsModel.comlist.comportA),
                  //  param => StartListeningCanExecute(_SerialPort));
                return _EnablesUseComA;
            }
        }
        public ICommand EnablesUseComB
        {
            get
            {
                _EnablesUseComB = new RelayCommand(
                             param => math_EnablesUseCom(IsUsedComB, SerialPortComB, SerialPortSettingsModel.comlist.comportB));
                return _EnablesUseComB;
            }
        }
        public ICommand EnablesUseComC
        {
            get
            {
                _EnablesUseComC = new RelayCommand(
                             param => math_EnablesUseCom(IsUsedComC, SerialPortComC, SerialPortSettingsModel.comlist.comportC));
                return _EnablesUseComC;
            }
        }
        public ICommand EnablesUseComD
        {
            get
            {
                _EnablesUseComD = new RelayCommand(
                             param => math_EnablesUseCom(IsUsedComD, SerialPortComD, SerialPortSettingsModel.comlist.comportD));
                return _EnablesUseComD;
            }
        }
        public ICommand EnablesUseComE
        {
            get
            {
                _EnablesUseComE = new RelayCommand(
                             param => math_EnablesUseCom(IsUsedComE, SerialPortComE, SerialPortSettingsModel.comlist.comportE));
                return _EnablesUseComE;
            }
        }
        #endregion

        #region Private DDS Meth
        public void math_Loadfullfunctionscript()
        {
           
            Items = fullFuncion.Instance.GetOBC_askform();
            OnPropertyChanged("Items");
            OnPropertyChanged("Loadfullfunctionscript");
        }

        public void math_EnablesUseCom(bool IsUsedComflag, SerialPort Comm, SerialPortSettingsModel.comlist _Func)
        {
            if (IsUsedComflag)
            {             
                StartListening(Comm, _Func);
                StartListeningCanExecute(Comm);
            }
            else
            {                
                StopListening(Comm, _Func);             
            }           
        }

        #endregion
        #region Public ICommands
        public ICommand Open
        {
            get
            {

                _Open = new RelayCommand(
                    param => StartListening(_SerialPort, SerialPortSettingsModel.comlist.comportA),
                    param => StartListeningCanExecute(_SerialPort));
                return _Open;
            }
        }    
        public ICommand Close
        {
            get
            {
                _Close = new RelayCommand(
                    param => StopListening(_SerialPort, SerialPortSettingsModel.comlist.comportA),
                    param => _SerialPort != null && _SerialPort.IsOpen);
                return _Close;
            }
        }   

        public ICommand Send
        {
            get
            {
                _Send = new RelayCommand(
                    param => WriteDataInterface(SelectedTestCom),
                    param => SerialPortComA != null && SerialPortComA.IsOpen);
                return _Send;
            }
        }   
        public ICommand Clear
        {
            get
            {
                _Clear = new RelayCommand(
                    param => OutputText = "");
                return _Clear;
            }
        }
        public ICommand OpenLink
        {
            get
            {
                _OpenLink = new RelayCommand(
                    param => System.Diagnostics.Process.Start("https://www.acepillar.com.tw/data/goods/201602/1454310017ZSY4J.pdf"));
                return _OpenLink;
            }
        }
        public ICommand RefreshPorts
        {
            get
            {
                _RefreshPorts = new RelayCommand(
                    param => RefreshPortsMethod());
                return _RefreshPorts;
            }
        }
        public ICommand ChangeFileLocation
        {
            get
            {
                _ChangeFileLocation = new RelayCommand(
                    param => ChangeFileLocationMethod());
                return _ChangeFileLocation;
            }
        } 
        public ICommand ExportFile
        {
            get
            {
                _ExportTXTFile = new RelayCommand(
                    param => ExportFileMethod(),
                    param => ExportFileCanExecute());
                return _ExportTXTFile;
            }
        }

        public ICommand RestoreSetCom
        {
            get
            {
                _RestoreSetCom = new RelayCommand(
                    param => reloadsetcom());
                return _RestoreSetCom;
            }
        }


        #endregion

        #region Events
        /// <summary>
        /// Receive data event from serial port.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void DataReceivedEvent(object sender, SerialDataReceivedEventArgs e)
        {
            if (_SerialPort.IsOpen)
            {
                try
                {
                    //string inData = _SerialPort.ReadLine();
                    //OutputText += inData.ToString() + SelectedLineEnding;
                    //OnPropertyChanged("OutputText");

                    byte[] data = new byte[_SerialPort.BytesToRead];
                    _SerialPort.Read(data, 0, data.Length);
                    string s = Encoding.GetEncoding("Windows-1252").GetString(data);
                  
                    OutputText += SerialPortSettingsModel.Instance.ReceivedDataTOInfoTable(_SerialPort, "ascii", s + SelectedLineEnding) + "\r\n";
                
                    OnPropertyChanged("OutputText");
                    logger.Log(LogLevel.Info, string.Concat("[MsgR] ", "[", _SerialPort.PortName, "] ", "[ascii] ", s + SelectedLineEnding));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    logger.Log(LogLevel.Error, "[Error] " + "[Nul] " + "EXCEPTION raised: " + ex.ToString());
                }
            }
        }
        private void TimerTick(object send, EventArgs e)
        {
            if (timer != null)
            {
                timer.Stop();
                timer = null;
                ExportStatus = "";
                OnPropertyChanged("ExportStatus");
            }
        }
        #endregion

        #region Public Methods
        public const string ItemsCollectionPropertyName = "ItemsCollection";
        private ObservableCollection<Model.SerialComSets> _itemsCollection = null;
        public ObservableCollection<Model.SerialComSets> ItemsCollection
        {
            get
            {
                return _itemsCollection;
            }
            set
            {
                if (_itemsCollection == value)
                {
                    return;
                }

                _itemsCollection = value;
                RaisePropertyChanged(ItemsCollectionPropertyName);
            }
        }
        private ObservableCollection<Model.SerialComSets> _selectedItems;
        public ObservableCollection<Model.SerialComSets> selectedRow
        {
            get
            {
                return _selectedItems;
            }
            set
            {
                _selectedItems = value;
                RaisePropertyChanged("SelectedItemss");
            }
        }
        private void reloadsetcom()
        { 
            col = new ObservableCollection<Model.SerialComSets>();
            foreach (var com in comidlist)
            {

                col.Add(new Model.SerialComSets()
                {
                    ComID = com,
                    comIsUse = false,
                    Com = 0,
                    BaudRates = 5,
                    Paritys = 0,
                    Databits = 3,
                    Stopbits = 0,
                    Flowcontrol = 0,
                    comIsDTR = true,
                    comIsRTS = true
                });

            }
            cvs.Source = col;

        }

        /// <summary>
        /// Close port if port is open when user closes MainWindow.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            try
            {
                if (_SerialPort != null && _SerialPort.IsOpen)
                {
                    logger.Log(LogLevel.Debug, "[Set] " + "[" + _SerialPort.PortName + "] " + "SerialPort.Dispose() & SerialPort.Close() are executed on OnWindowClosing() method.");
                    _SerialPort.DataReceived -= DataReceivedEvent;
                    _SerialPort.Dispose();
                    _SerialPort.Close();
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                logger.Log(LogLevel.Error, "[Error] " + "[Nul] " + "EXCEPTION raised: " + ex.ToString());
            }
        }
        #endregion

        #region Private Methods
 
        /// <summary>            
        ///   針對串列埠的訊息做發送以及紀錄的動作
        ///   參數為  (SerialPort _ComPort, byte[] _ByteData)
        /// </summary>
        /// <param name="_ComPort"></param>
        /// <param name="_ByteData"></param>
        private void RecordAndSendSerialData(SerialPort _ComPort, byte[] _ByteData)
        {
            try
            {
                if (_ByteData != null)
                {
                    _ComPort.Write(_ByteData, 0, _ByteData.Length);
                    string strmsg = Encoding.GetEncoding("Windows-1252").GetString(_ByteData);
                    string strdata = SerialPortSettingsModel.byteToHexStr(_ByteData);
                    OutputText += SerialPortSettingsModel.Instance.ReceivedDataTOInfoTable(_SerialPort, "ascii", strmsg ) + "\r\n";
                    OutputText += SerialPortSettingsModel.Instance.ReceivedDataTOInfoTable(_SerialPort, "hex", strdata ) + "\r\n";
                    OnPropertyChanged("OutputText");
             
                    logger.Log(LogLevel.Info, string.Concat("[Msg] ", "[", _ComPort.PortName, "] ", strmsg));
                    logger.Log(LogLevel.Info, string.Concat("[Msg] ", "[", _ComPort.PortName, "] ", strdata));
                    Thread.Sleep(30);
                }

            }
            catch
            { 
                //請補寫事件註解
            }
          
        }

        /// <summary>
        /// Send data to serial port.
        /// </summary>
        private void WriteDataInterface(SerialPortSettingsModel selectedcom)
        {
          
            SerialPort _ComPort;
            //SerialPortComD
            if (selectedcom.DeviceID == SerialPortComA.PortName)
            {
                _ComPort = SerialPortComA;
                WriteData(_ComPort);
            }

            else if (selectedcom.DeviceID == SerialPortComB.PortName)
            {
                _ComPort = SerialPortComB;
                WriteData(_ComPort);
            }
             
            else if (selectedcom.DeviceID == SerialPortComC.PortName)
            {
                _ComPort = SerialPortComC;
                WriteData(_ComPort);
            }
    
            else if (selectedcom.DeviceID == SerialPortComD.PortName)
            {
                _ComPort = SerialPortComD;
                WriteData(_ComPort);
            }
            
            else if (selectedcom.DeviceID == SerialPortComE.PortName)
            {
                _ComPort = SerialPortComE;
                WriteData(_ComPort);
            }
        

           
        }

        private void WriteData(SerialPort _ComPort)
        {
           
            if (_ComPort.IsOpen)
            {
                try
                {
                    if (InputText.Contains("$"))
                    {
                        string subInputText = InputText.Replace("$", " ");

                        subInputText += " 0D 0A";
                        subInputText = subInputText.Substring(1, subInputText.Length - 1);
                        byte[] hexbytes = ConvertMsg.Instance.HexStringToBytes(subInputText);
                        _ComPort.Write(hexbytes, 0, hexbytes.Length);
                    }
                    else
                    {
                        _ComPort.Write(InputText + "\r\n");

                    }

                    logger.Log(LogLevel.Info, string.Concat("[Msg] ", "[", _ComPort.PortName, "] ", InputText));
                    OnPropertyChanged("InputText");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    logger.Log(LogLevel.Error, "[Error] " + "[Nul] " + "EXCEPTION raised: " + ex.ToString());
                }
            }
        }

        /// <summary>
        /// Initiate serial port communication.
        /// </summary>
        /// 
        public bool IsValid
        {
            get
            {
                bool check = true;
                string ww = "!";
                MessageBox.Show(ww);
                return check;
            }
        }


        /// 
        /// <summary>
        /// Initiate serial port communication.
        /// </summary>
        private void StartListening(SerialPort _ComPort, SerialPortSettingsModel.comlist _Func)
        {
            try
            {
                if (_ComPort != null && _ComPort.IsOpen)
                {
                    logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "SerialPort.Dispose() & SerialPort.Close() are executed on StartListening() method.");
                    _ComPort.Dispose();
                    _ComPort.Close();
                    logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "The action done for SerialPortDispose.");
                }

                if (SelectedCommPort != null)
                {
                    switch (_Func)
                    {
                        case SerialPortSettingsModel.comlist.comportA:
                            _ComPort.PortName = SelectedComA.DeviceID;
                            EnableDisableComASettings = false;                         
                            _ComPort.DataReceived += new SerialDataReceivedEventHandler(comportADataReceive);
                            OnPropertyChanged("EnableDisableComASettings");
                            break;
                        case SerialPortSettingsModel.comlist.comportB:
                            _ComPort.PortName = SelectedComB.DeviceID;
                            EnableDisableComBSettings = false;
                            OnPropertyChanged("EnableDisableComBSettings");
                            break;
                        case SerialPortSettingsModel.comlist.comportC:
                            _ComPort.PortName = SelectedComC.DeviceID;
                            EnableDisableComCSettings = false;
                            OnPropertyChanged("EnableDisableComCSettings");
                            break;
                        case SerialPortSettingsModel.comlist.comportD:
                            _ComPort.PortName = SelectedComD.DeviceID;
                            EnableDisableComDSettings = false;
                            OnPropertyChanged("EnableDisableComDSettings");
                            break;
                        case SerialPortSettingsModel.comlist.comportE:
                            _ComPort.PortName = SelectedComE.DeviceID;
                            EnableDisableComESettings = false;
                            OnPropertyChanged("EnableDisableComESettings");
                            break;
                   
                    }
                    switch (_Func)
                    {
                        case SerialPortSettingsModel.comlist.comportA:
                            _ComPort.BaudRate = SelectedComABaudRate;
                            break;
                        case SerialPortSettingsModel.comlist.comportB:
                            _ComPort.BaudRate = SelectedComBBaudRate;
                            break;
                        case SerialPortSettingsModel.comlist.comportC:
                            _ComPort.BaudRate = SelectedComCBaudRate;
                            break;
                        case SerialPortSettingsModel.comlist.comportD:
                            _ComPort.BaudRate = SelectedComDBaudRate;
                            break;
                        case SerialPortSettingsModel.comlist.comportE:
                            _ComPort.BaudRate = SelectedComEBaudRate;
                            break;

                    }

                    _ComPort.Parity = SelectedParity;
                    _ComPort.DataBits = SelectedDataBits;
                    _ComPort.StopBits = SelectedStopBits;
                    _ComPort.DtrEnable = IsDTR;
                    _ComPort.RtsEnable = IsRTS;
                    _ComPort.Handshake = Handshake.None;
                    _ComPort.Open();

                    switch (_Func)
                    {
                        case SerialPortSettingsModel.comlist.comportA:
                            _ComPort.DataReceived += new SerialDataReceivedEventHandler(comportADataReceive);                           
                            break;
                        case SerialPortSettingsModel.comlist.comportB:
                            break;
                        case SerialPortSettingsModel.comlist.comportC:
                            break;
                        case SerialPortSettingsModel.comlist.comportD:
                            break;
                        case SerialPortSettingsModel.comlist.comportE:
                            break;

                    }

                    logger.Log(LogLevel.Debug, "[Set], " + "["+ _ComPort.PortName + "] "+ " SerialPort.Open() is executed.");
                    OutputText = "";
                    OnPropertyChanged("OutputText");
                
                    logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "Connected to: " + 
                        SelectedCommPort.DeviceID + ", " + SelectedBaudRate.ToString() + " baud, Parity." + SelectedParity.ToString() + ", " 
                        + SelectedDataBits.ToString() + ", StopBits." + SelectedStopBits.ToString() + ", RTS=" + IsRTS.ToString() + ", DTR=" + IsDTR.ToString());
                    logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "Ready to receive data...");

                }
                else
                {
                    logger.Log(LogLevel.Error, "[Set] " + "[Nul] " + "not a comport in there.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                logger.Log(LogLevel.Error, "[Set] " + "[Nul] " + "EXCEPTION raised: " + ex.ToString());
            }

            WindowTitle = AppTitle + " (" + GetConnectionStatus(_ComPort) + ")";
            OnPropertyChanged("WindowTitle");
        }

        /// <summary>
        /// Allow/disallow StartListening() to be executed.
        /// </summary>
        /// <returns>True/False</returns>
        private bool StartListeningCanExecute(SerialPort _ComPort)
        {
            if (CommPorts == null)
            {
                return false;
            }
            else
            {
                if (_ComPort == null || !_ComPort.IsOpen)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Terminate serial port communication.
        /// </summary>
        private void StopListening( SerialPort _ComPort, SerialPortSettingsModel.comlist _Func)
        {
            if (_ComPort != null && _ComPort.IsOpen)
            {
                try
                {
               
                    switch (_Func)
                    {
                        case SerialPortSettingsModel.comlist.comportA:
                            logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "Disconnected from " + SelectedComA.DeviceID);
                            EnableDisableComASettings = true;
                            OnPropertyChanged("EnableDisableComASettings");
                            break;
                        case SerialPortSettingsModel.comlist.comportB:
                            logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "Disconnected from " + SelectedComB.DeviceID);
                            EnableDisableComBSettings = true;
                            OnPropertyChanged("EnableDisableComBSettings");
                            break;
                        case SerialPortSettingsModel.comlist.comportC:
                            logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "Disconnected from " + SelectedComC.DeviceID);
                            EnableDisableComCSettings = true;
                            OnPropertyChanged("EnableDisableComCSettings");
                            break;
                        case SerialPortSettingsModel.comlist.comportD:
                            logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "Disconnected from " + SelectedComD.DeviceID);
                            EnableDisableComDSettings = true;
                            OnPropertyChanged("EnableDisableComDSettings");
                            break;
                        case SerialPortSettingsModel.comlist.comportE:
                            logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "Disconnected from " + SelectedComE.DeviceID);
                            EnableDisableComESettings = true;
                            OnPropertyChanged("EnableDisableComESettings");
                            break;

                    }
                    //https://www.codeproject.com/Questions/1247211/Unsubscribing-from-an-event-still-its-getting-fire

                    logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + _ComPort.IsOpen.ToString() + " ~~~~~~~~1~~~~~~~~");
                    _ComPort.DataReceived -= DataReceivedEvent;
                    _ComPort.DataReceived -= new SerialDataReceivedEventHandler(comportADataReceive);               
                    logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + _ComPort.IsOpen.ToString() + " ~~~~~~~~2~~~~~~~~");
                    _ComPort.Dispose();
                    logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + _ComPort.PortName + " ~~~~~~~~3~~~~~~~~");
                    _ComPort.Close();
                    logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + _ComPort.PortName + " ~~~~~~~~4~~~~~~~~");


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    logger.Log(LogLevel.Error, "[Error] " + "[Nul] " + "EXCEPTION raised: " + ex.ToString());
                }
            }

            WindowTitle = AppTitle + " (" + GetConnectionStatus(_ComPort) + ")";
            OnPropertyChanged("WindowTitle");

        }

        /// <summary>
        /// Get connection/communication status.
        /// </summary>
        /// <returns>String of Connected/Disconnect</returns>  
        private string GetConnectionStatus(SerialPort _ComPort)
        {
            if (_ComPort != null && _ComPort.IsOpen)
                return "Connected";
            else
                return "Not Connected";
        }

        /// <summary>
        /// Rescan avaiable ports
        /// </summary>
        private void RefreshPortsMethod()
        {
            try
            {
                CommPorts = SerialPortSettingsModel.Instance.getCommPorts();
                OnPropertyChanged("CommPorts");
                SelectedCommPort = CommPorts[0];
                OnPropertyChanged("SelectedCommPort");
                logger.Log(LogLevel.Debug, "[Set] " + "New list of COM* ports are repopulated.");
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Error, "[Set] " + "EXCEPTION raised: " + ex.ToString());
            }
        }
        private void ChangeFileLocationMethod()
        {
            var dlg = new CommonOpenFileDialog();
            dlg.Title = "File Location";
            dlg.IsFolderPicker = true;
            dlg.InitialDirectory = FileLocation;

            dlg.AddToMostRecentlyUsedList = false;
            dlg.AllowNonFileSystemItems = false;
            dlg.DefaultDirectory = FileLocation;
            dlg.EnsureFileExists = true;
            dlg.EnsurePathExists = true;
            dlg.EnsureReadOnly = false;
            dlg.EnsureValidNames = true;
            dlg.Multiselect = false;
            dlg.ShowPlacesList = true;

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                var folder = dlg.FileName;
                // Do something with selected folder string
                FileLocation = folder.ToString();
                OnPropertyChanged("FileLocation");
            }

        }
        private void ExportFileMethod()
        {
            try
            {
                if (File.Exists(FileLocation + @"\" + FileName + SelectedFileExtension))
                {
                    MessageBoxResult msgBoxResult = MessageBox.Show(
                        "File " + FileName + SelectedFileExtension + " already exists!\n Select 'Yes' to overwrite the existing file or\n'No' to create a new file with timestamp suffix or\n 'Cancel' to cancel?",
                        "Overwrite Confirmation",
                        MessageBoxButton.YesNoCancel);
                    if (msgBoxResult == MessageBoxResult.Yes)
                    {
                        File.WriteAllText(FileLocation + @"\" + FileName + SelectedFileExtension, OutputText);
                        logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "Output data is saved and exported into " + FileLocation + @"\" + FileName + SelectedFileExtension);
                        ExportStatus = "Done.";
                        OnPropertyChanged("ExportStatus");
                        ExportStatusSuccess = true;
                        OnPropertyChanged("ExportStatusSuccess");
                        StartTimer(10);

                    }
                    else if (msgBoxResult == MessageBoxResult.No)
                    {
                        File.WriteAllText(FileLocation + @"\" + FileName + DateTime.Now.ToString("-yyyyMMddHHmmss") + SelectedFileExtension, OutputText);
                        logger.Log(LogLevel.Debug, "[Set] " + "[Nul] " + "Output data is saved and exported into " + FileLocation + @"\" + FileName + DateTime.Now.ToString("-yyyyMMddHHmmss") + SelectedFileExtension);
                        ExportStatus = "Done.";
                        OnPropertyChanged("ExportStatus");
                        ExportStatusSuccess = true;
                        OnPropertyChanged("ExportStatusSuccess");
                        StartTimer(10);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    File.WriteAllText(FileLocation + @"\" + FileName + SelectedFileExtension, OutputText);
                    logger.Log(LogLevel.Error, "[Set] " + "[Nul] " + "Output data is saved and exported into " + FileLocation + @"\" + FileName + SelectedFileExtension);
                    ExportStatus = "Done.";
                    OnPropertyChanged("ExportStatus");
                    ExportStatusSuccess = true;
                    OnPropertyChanged("ExportStatusSuccess");
                    StartTimer(10);
                }
            }
            catch (Exception ex)
            {
                ExportStatus = "Error exporting a file!";
                OnPropertyChanged("ExportStatus");
                ExportStatusSuccess = false;
                OnPropertyChanged("ExportStatusSuccess");
                StartTimer(10);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                logger.Log(LogLevel.Error, "[Error] " + "EXCEPTION raised: " + ex.ToString());
            }
        }
        private bool ExportFileCanExecute()
        {
            return FileName != "";
        }
        private void StartTimer(int duration)
        {
            if (timer != null)
            {
                timer.Stop();
                ExportStatus = "";
                OnPropertyChanged("ExportStatus");
            }
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(duration);
            timer.Tick += new EventHandler(TimerTick);
            timer.Start();
        }
        #endregion


        private void comportADataReceive(object sender, SerialDataReceivedEventArgs e)
        {
            int BufferNum = 64;
            Byte[] buffer = new Byte[BufferNum];
            Byte[] ACKbuffer = new Byte[3];
            int MsgIndex = 0;
            int ACKIndex = 0;
            byte _byte = 0x00;
            bool StartMsg = false;
            bool StartACK = false;
            string data = null;
            SerialPort _ComPort = SerialPortComA;

            while (!EnableDisableComASettings)
            {
                if (_ComPort.IsOpen)
                {
                    while (_ComPort.BytesToRead > 0)
                    {
                        try
                        {
                            int ind = 0;
                            foreach (var New in buffer.Select((value, index) => new { value, index }))
                            {
                                ind = New.index;
                                _byte = (byte)_ComPort.ReadByte();         //读取数据
                                if (_byte == 0xC3 & StartMsg == false)
                                {
                                    //ack
                                    StartACK = true;
                                    buffer[ind] = _byte;
                                    ACKbuffer[ind] = _byte;
                                }
                                else if (StartACK == true)
                                {
                                    buffer[ind] = _byte;
                                    if (buffer[ind] == 0x0A)
                                    {
                                        StartACK = false;
                                        break;
                                    }
                                }
                                else if (StartMsg == false)
                                {
                                    StartMsg = true;
                                    buffer[ind] = _byte;
                                }
                                else if (StartMsg == true && New.index > 0)
                                {
                                    buffer[ind] = _byte;
                                    if (buffer[ind - 1] == 0x0D && buffer[ind] == 0x0A)
                                    {
                                        StartMsg = false; //有效資料讀取結束

                                        Byte[] buffer2 = new Byte[New.index];

                                        List<byte> Subbuffer = new List<byte>();
                                        foreach (var _sub in buffer.Select((value, index) => new { value, index }))
                                        {

                                            if (New.index == _sub.index)
                                            {
                                                Subbuffer.Add(_sub.value);
                                                byte[] byteSubBuffer = Subbuffer.ToArray();

                                                string strmsg = Encoding.GetEncoding("Windows-1252").GetString(byteSubBuffer);
                                                data = SerialPortSettingsModel.byteToHexStr(byteSubBuffer);
                                                ///For more analytic Method, write below

                                                OutputText += SerialPortSettingsModel.Instance.ReceivedDataTOInfoTable(_SerialPort, "ascii", strmsg + "\r\n") + "\r\n";
                                              
                                                OnPropertyChanged("OutputText");
                                                logger.Log(LogLevel.Info, string.Concat("[MsgR] ", "[", _ComPort.PortName, "] ", "[ascii] ", strmsg));

                                                OutputText += SerialPortSettingsModel.Instance.ReceivedDataTOInfoTable(_SerialPort, "hex", data + "\r\n") + "\r\n";
                                               
                                                OnPropertyChanged("OutputText");
                                                logger.Log(LogLevel.Info, string.Concat("[MsgR] ", "[", _ComPort.PortName, "] ", "[hex] ", data));

                                            }
                                            else
                                            {
                                                Subbuffer.Add(_sub.value);
                                            }

                                        }
                                        break;
                                    }
                                }
                                else
                                {
                                    buffer[ind] = _byte;
                                    if (New.index == BufferNum - 1)
                                    {
                                        buffer = new Byte[BufferNum];
                                        break;

                                    }
                                }
                            }

                        }
                        catch (TimeoutException)
                        {
                            MessageBox.Show("readbyte overrun");
                        }
                    }
                }
            }
        }

    }

}
