﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;

namespace DDSbeta2.ViewModel
{
    class InterfaceControlled : ViewModelBase
    {
        public InterfaceControlled()
        {

        }

        private ICommand _testlinked;
        public ICommand testlinked
        {
            get
            {

                _testlinked = new RelayCommand(
                     param => WriteData());

                return _testlinked;
            }
        }
        public void WriteData()
        {
            MessageBox.Show("readbyte overrun");
        }
        //    <Button Content="點位置換算"  Binding="{Binding testlinked}" Width="75"/>
        //
    }
}
